% CTA_TRAIN training of the Fourier CHOG filter described in  
%
% Henrik Skibbe and Marco Reisert 
% "Circular Fourier-HOG Features for Rotation Invariant Object Detection in Biomedical Images"
% in Proceedings of the IEEE International Symposium on Biomedical Imaging 2012 (ISBI 2012), Barcelona 
% https://bitbucket.org/skibbe/fourierchog/
%
% model=cta_train(images,...                    % cell array images
%                 labels,...                    % cell array of label images
%                 'L',5,...                     % maximum angular frequency
%                 'v_sigma',...                 % scale of the voting function    
%                 'precision','double',...      % precision ('double'/'single')
%                 'chog_options',{'l2',true});  % see sta_chog
%
%
% returns a filter model. Use the model in combinaion with cta_apply to
% apply the trained filter to new images
%
%
% See also cta_chog cta_invrts cta_apply


%
% Copyright (c) 2011, Henrik Skibbe and Marco Reisert
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met: 
% 
% 1. Redistributions of source code must retain the above copyright notice, this
%    list of conditions and the following disclaimer. 
% 2. Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution. 
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
% The views and conclusions contained in the software and documentation are those
% of the authors and should not be interpreted as representing official policies, 
% either expressed or implied, of the FreeBSD Project.
%


function [model,fimages] = cta_train2(images,labels,varargin)


if ~iscell(images)
    images={images};
end;

if ~iscell(labels)
    labels={labels};
end;


verbosity=0;
precision='double';
%w_func={'circle',[0,2.5],[5,2.5]};
v_sigma=6;
chog_options={'w_func',{'circle',[0,2.5],[2.5,2.5],[5,2.5]}};
product_options={'monoms',{'001'}};
output_order=0;
L=5;

regu=0.00001;

%filter_mode='replicate';
filter_mode='circular';



for k = 1:2:numel(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


product_options=[product_options,'output_order',output_order];


complex_derivatives=...
           [0,0,(-1i)/8,0,0;
           0,0,1i,0,0;
           (-1)/8,1 ,0,-1, (1)/8 ;
           0,0,-1i,0,0;
           0,0,(1i)/8,0,0;];
       
complex_derivatives=conj(complex_derivatives);  



model.v_sigma=v_sigma;
model.product_options=product_options;
model.L=L;
model.filter_mode=filter_mode;
model.chog_options=chog_options;
model.output_order=output_order;

FIcol = []; %% features
licol = []; %% targets
nVcol = 0;  %% number of voxels per image

for nI = 1:length(images),
    if verbosity>0
        fprintf('computing features for image %d\n',nI);
    end;
    img = images{nI}; 
    label = labels{nI};
    if output_order>0
        label=label.^output_order;
    end;
    
    shape=size(img);
    
    chog=cta_chog(img,'L',L,'precision',precision,model.chog_options{:});

    if nI==1
        product_mat=cta_products(chog,product_options{:});
    end;
    
    num_products=size(product_mat,1);

    
    num_voting_finc=numel(v_sigma);
    
    
    fimage=zeros([prod(shape),num_voting_finc*num_products],precision);
    
    
    count=1;
    
    for vf=1:num_voting_finc,
        ft_kernel{vf}=fftn(cta_fspecial('gauss',v_sigma(vf),shape,false,precision));
    end;
    
        
        for p=1:num_products,
            product=product_mat(p,:);
            if product(3)
                A=conj(squeeze(chog{product(1)}.data(product(2)+1,:,:)));
            else
                A=(squeeze(chog{product(1)}.data(product(2)+1,:,:)));
            end;



            if (product(4)==-1)
                    if verbosity>1
                     fprintf('(%d) [%d]%d  -> %d\n',product(1),(-1)^product(3),product(2),product(2));
                    end;
                    tmp=A;
            else
                if product(6)
                    B=conj(squeeze(chog{product(4)}.data(product(5)+1,:,:)));
                else
                    B=(squeeze(chog{product(4)}.data(product(5)+1,:,:)));
                end;  

                if (product(7)==-1)
                        if verbosity>1
                            fprintf('(%d) [%d]%d x (%d) [%d]%d -> %d\n',product(1),(-1)^product(3),product(2),product(4),(-1)^product(6),product(5),product(10));
                        end;
                        tmp=A.*B;
                else
                    if product(9)

                        C=conj(squeeze(chog{product(7)}.data(product(8)+1,:,:)));
                    else
                        C=(squeeze(chog{product(7)}.data(product(8)+1,:,:)));
                    end;
                        if verbosity>1
                          fprintf('(%d) [%d]%d x (%d) [%d]%d x (%d) [%d]%d -> %d\n',product(1),(-1)^product(3),product(2),product(4),(-1)^product(6),product(5),product(7),(-1)^product(9),product(8),product(10));
                        end;
                        tmp=A.*B.*C;
                end;
            end;

            l=product(end-1);
            assert(l>=output_order);
            while (l>output_order)
                l=l-1;
                tmp=imfilter(tmp,complex_derivatives,filter_mode);
            end;

            for vf=1:num_voting_finc,
                tmp=ifftn(fftn(tmp).*ft_kernel{vf});
                Mask=zeros(shape);
                border=ceil(max(v_sigma)/2);
                Mask(border:end-border+1,border:end-border+1)=1;
                tmp(Mask==0)=0;
                fimage(:,(vf-1)*num_products+count)=tmp(:);
            end;

            count=count+1;      

        end;

    
      FIcol = cat(1,FIcol,fimage);
      licol = [licol ; label(:)];

      nVcol = nVcol + prod(shape);

end;

if verbosity>0
    fprintf('solving regression problem ... ');
end;
    renormfac = sqrt(sum(abs(FIcol).^2,1)/nVcol)';
    invrenormfac = 1./renormfac;
    FIcol = FIcol.* (invrenormfac*ones(1,nVcol))';
    Corr = FIcol'*FIcol;
    b = FIcol'*double(licol(:));
    alpha = (Corr+regu*diag(invrenormfac.^2))\b;
    alpha = alpha / (alpha'*b) *sum(abs(licol(:)));


nvf_coeff=numel(alpha)/num_voting_finc;    
nalpha=invrenormfac.*alpha;
for vf=1:num_voting_finc    
    model.alpha{vf}=nalpha((vf-1)*nvf_coeff+1:(vf)*nvf_coeff);
end;
model.products=product_mat;

if verbosity>0
    fprintf('done\n');
end;


if (verbosity>0) || (nargout>1)
    
    current=1;
    if (verbosity>0)    
        figure(4567+output_order);
        clf;
        hold on;
    end;
    for a=1:numel(images),
        shape=size(images{a});
        if nargout>1
            fimages{a}=reshape(FIcol(current:current+prod(shape)-1,:),[shape,size(FIcol,2)]);
        end;
        
        if (verbosity>0)    
            switch (output_order)
                case 0
                    result=real(reshape(FIcol(current:current+prod(shape)-1,:)*alpha,(shape)));
                    if numel(images)==1
                        imagesc(result.*(result>0));
                    else
                        subplot(1,numel(images),a);
                        imagesc(result.*(result>0));
                    end;
                    xlabel(['self detection: training image ',num2str(a)]);
                    axis image;
                otherwise
                    result=reshape(FIcol(current:current+prod(shape)-1,:)*alpha,(shape));
                    result=result.^(1/output_order);
                    if numel(images)==1
                        imagesc(images{a});
                        colormap gray;
                        [X,Y]=meshgrid(1:2:size(result,2),1:2:size(result,1));
                        quiver(X,Y,real(result(1:2:end,1:2:end)),imag(result(1:2:end,1:2:end)),'r','linewidth',1);
                    else
                        subplot(1,numel(images),a);
                        colormap gray;
                        [X,Y]=meshgrid(1:2:size(result,2),1:2:size(result,1));
                        quiver(X,Y,real(result(1:2:end,1:2:end)),imag(result(1:2:end,1:2:end)),'r','linewidth',1);
                    end;
                    xlabel(['self detection: training image ',num2str(a)]);
                    axis image;
            end;
        end;
        current=current+prod(shape);
    end;
end;        




