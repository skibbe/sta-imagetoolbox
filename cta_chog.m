% CTA_CHOG computes Circular Fourier HOG features according to eq. (3) in 
%
% Henrik Skibbe and Marco Reisert 
% "Circular Fourier-HOG Features for Rotation Invariant Object Detection in Biomedical Images"
% in Proceedings of the IEEE International Symposium on Biomedical Imaging 2012 (ISBI 2012), Barcelona 
%
% chog=cta_chog(image,...                       % NxM image
%               'w_func',{'circle',[4,2]},...   % window function(s), e.g. {'circle',[0,3],[3,3],[6,3]}
%               'L',5,...                       % maximum angular frequency
%               'precision','double',...        % precision ('double'/'single')
%               'presmooth','1.5',...           % smoothing before computing the gradient
%               'l2',true,...                   % l2-normalization     
%               'gamma',0.8);                   % gamma corection of gradient magnitude
%
% returns a list (one element for each window functions)
%
%     chog{w}.data      % CxNxM image containing the C expansion coefficients        
%     chog{w}.L         % maximum angular frequency
%     chog{w}.shape     % [N,M]
%
%
% Note that these features are NOT rotation invariant
%
% See also cta_invrts cta_train cta_apply


%
% Copyright (c) 2011, Henrik Skibbe and Marco Reisert
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met: 
% 
% 1. Redistributions of source code must retain the above copyright notice, this
%    list of conditions and the following disclaimer. 
% 2. Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution. 
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
% The views and conclusions contained in the software and documentation are those
% of the authors and should not be interpreted as representing official policies, 
% either expressed or implied, of the FreeBSD Project.
%

function chog=cta_chog(Image,varargin)

gamma=0.8;
L=5;
verbosity=0;

w_func={'circle',[4,2]};

presmooth=1.5;
l2=true;

%filter_mode='replicate';
filter_mode='circular';

precision='double';

complex_derivatives=...
           [0,0,(-1i)/8,0,0;
           0,0,1i,0,0;
           (-1)/8,1 ,0,-1, (1)/8 ;
           0,0,-1i,0,0;
           0,0,(1i)/8,0,0;];
       
%complex_derivatives=conj(complex_derivatives);       

for k = 1:2:numel(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;
    

shape=size(Image);
image=cast(Image,precision);


if presmooth>0,    
    ft_kernel=fftn(cta_fspecial('gauss',presmooth,shape,false,precision));
    image=ifftn(fftn(image).*ft_kernel);
end;


% computing the complex derivatives df/dx+idf/dy (paper text below eq. (4))
gradient_image=imfilter(image,complex_derivatives,filter_mode);

% computing the gradient magnitude
gradient_magnitude=abs(gradient_image);
inv_gradient_magnitude=1./(gradient_magnitude+eps);

% gamma correction (paper eq. (4))
if gamma~=1
    gradient_magnitude=gradient_magnitude.^gamma;
end;


% computing gradient orientation ^g (paper text below eq. (1))
gradient_direction=gradient_image.*inv_gradient_magnitude;


Fourier_coefficients=complex(zeros([L,shape],precision));


% iterative computation of the coefficients e^l(^g) (paper eq. (3))
Fourier_coefficients(1,:,:)=gradient_magnitude;
Fourier_coefficients(2,:,:)=gradient_direction.*gradient_magnitude;

current=gradient_direction;
for l=3:L+1,
    current=current.*gradient_direction;
    Fourier_coefficients(l,:,:)=current.*gradient_magnitude;
end;

% computung a^l_w(x) : convoluion with window function(s) (paper eq. (3))
if (numel(w_func)>0)
    for w=1:numel(w_func)-1
            if (l2)
                tmp2=zeros(shape);
            end;
        
            chog{w}.data=zeros([size(Fourier_coefficients,1),shape],precision);
            chog{w}.L=L;
            chog{w}.shape=shape;
            chog{w}.w_func=w_func{1};
            chog{w}.w_params=w_func{w+1};
            if verbosity>0
                wf=cta_fspecial(w_func{1},w_func{w+1},shape,true,precision);
                figure(1234);
                subplot(1,numel(w_func)-1,w);
                imagesc(wf);
                axis image;
                xlabel(['window func. ',num2str(w)]);
            end;
            
            wf=cta_fspecial(w_func{1},w_func{w+1},shape,false,precision);
            ft_kernel=fftn(wf);

            for l=1:L+1,
                tmp=ifftn(fftn(squeeze(Fourier_coefficients(l,:,:))).*ft_kernel);
                chog{w}.data(l,:,:)=tmp;
                 if (l2)
                     tmp2=tmp2+squeeze(real(chog{w}.data(l,:,:)).^2+imag(chog{w}.data(l,:,:)).^2);
                 end;
            end;
            if (l2)
                tmp2=sqrt(tmp2)+eps;
                chog{w}.data=reshape(reshape(chog{w}.data,[L+1,shape(1)*shape(2)])./repmat(reshape(tmp2(:),[1,size(tmp2(:),1)]),L+1,1),[L+1,shape]);
            end;
    end;
else
    chog.data=Fourier_coefficients;
    chog.L=L;
    chog{w}.shape=shape;
end;


