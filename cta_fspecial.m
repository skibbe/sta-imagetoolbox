function kernel=cta_fspecial(kname,kparams,shape,centered,precision)

assert(numel(shape)==2);

if nargin<4
    centered=false;
end;
if nargin<5
    precision='single';
end;

[X Y] = ndgrid(0:(shape(1)-1),0:(shape(2)-1));
X = cast(X - ceil(shape(1)/2),precision);
Y = cast(Y - ceil(shape(2)/2),precision);

if strcmp(kname,'gauss')
    assert(numel(kparams)==1);
    sigma=2*kparams(1)^2;
    R2 = (X.^2./sigma + Y.^2./sigma);
    kernel=exp(-R2);
    kernel=kernel./sum(kernel(:));
elseif strcmp(kname,'circle')
    radius=kparams(1);
    radial_smooth=2*kparams(2)^2;
    distance_sqr = sqrt(X.^2 + Y.^2);
    R2 = ((distance_sqr-radius).^2)/radial_smooth;
    kernel=exp(-R2);
    kernel=kernel./sum(kernel(:));
else
    error('unsupported kernel');
end;


if ~centered
    kernel = fftshift(kernel);
end;

