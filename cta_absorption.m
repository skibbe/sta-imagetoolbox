function img=cta_absorption(img,absorb,noise)

shape=size(img);

texture=double(rgb2gray(imread('texture1.png')))/255;
img=0.7*img+0.3*texture(1:shape(1),1:shape(2));

[X Y] = ndgrid(0:(shape(1)-1),0:(shape(2)-1));
Y=Y./shape(2);

img=img.*exp(-Y*absorb);

max_value=max(img(:));
img=img+noise*max_value*randn(shape);
